# Stamps

## Overview
Here I show designs of some of my 3d-printed stamps.   
Each stamp is made of 2 parts: the motive part is printed from a flexible material (TPU) and the handle part is printed from rigid material (PLA).
One advantage of this is that you can use one handle for multiple motives of the same size.
The folder structure of the files here reflects this grouping by size and format.

## Modeling
The design of the motive is done in [inkscape](https://inkscape.org/), and the creation of the 3d .stl file in [tinkercad](https://www.tinkercad.com/).  
The motive part is compost of three layers:
- the first layer of the motive part is the actual motive 
- the second layer is a background for stability and connection of the motive segments 
- the third layer is a connector to the handle   

The layout that has worked for me is to have the first layer 3mm high, the second layer 2 mm high, and the connector parts 4 mm high.

The handle is similarly built in layers, with the lowest one having the connector holes, and the following layers making the shape of a handle.
The connector holes in the handle are 5 mm high, and 3% wider in the x and y directions than the connector parts on the motive.

## Hardware
- we have the Bambu Lab X1-Carbon Combo 3D Printer
- 0.4 mm nozzle
- TPU filament: TPU 95A HF
- PLA filament: some standard one

## Notes on the outcome
Generally the design and the print work very well.
A couple notes on design features:
- As my motive was printed on textured plate, the surface was not perfectly plane. 
With some light sanding, however, this can be mitigated.
I usually kept some of the coarser structure of the surface because I like the vibe of it. :)
- The motive is designed with a overhang (of the background layer to the motive layer).  
For stamps that have a frame on the outside of the motive the overhang is supported on all sides `|^^^|`.
The print of the first overhang layer is not pretty, but for me it never hindered the functionality of the stamp, given that the screwed-up layer is well above the stamp's surface.  
For stamps that do not have a frame on the outside of the motive, it is a bit more tricky and the design of the motive background layer has to be more careful.
- In general the resolution of the motive is limited by the print resolution.
The limited resolution can be noticed in the smaller 13 mm round stamps, but for larger motives it has not been a problem so far.

## Gallery

### 13 mm round stamps
![croissant model bottom](./13mm_round/images/croissant_model_bottom.jpg)
![croissant model top](./13mm_round/images/croissant_model_top.jpg)  
![moon 01](./13mm_round/images/moon_01.jpg)
![moon 02](./13mm_round/images/moon_02.jpg)
![several](./13mm_round/images/several.jpg)

### 75 x 50 mm rectangular text stamps
![AGzG front](./75x50mm_rectangular_text/images/AGzG_01_front.jpg)
![AGzG back](./75x50mm_rectangular_text/images/AGzG_01_back.jpg)
![AGzG bent](./75x50mm_rectangular_text/images/AGzG_01_bent.jpg)


## License
<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Creative Commons Attribution-NonCommercial 4.0 International
<br>
<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1" alt=""></a></p> 

> This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only. 