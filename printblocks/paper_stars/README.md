#  Paper Stars 

### Modeling
The design of the motive was first drawn on a tablet and then converted to a path in [inkscape](https://inkscape.org/).
The 3d .stl file was constructed in [tinkercad](https://www.tinkercad.com/).  
The print plate is compost of two layers:
- the first layer is the motive 
- the second layer is a solid background

### Hardware
- we have the Bambu Lab X1-Carbon Combo 3D Printer
- 0.4 mm nozzle
- smooth print plate
- TPU filament: TPU 95A HF

### Results
A more detailed description of the paper star making with the print plate is located here: [https://judiths.website/gallery/2024-12-10-paper_star_printing/](https://judiths.website/gallery/2024-12-10-paper_star_printing/).

![printblock](./images/paper_star_printblock.jpg)
![paper stars](./images/paper_star_v01_picture_smaller.jpg)


## License
<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Creative Commons Attribution-NonCommercial 4.0 International
<br>
<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1" alt=""></a></p> 

> This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only. 